package jono.controller;

import com.google.common.collect.ImmutableMap;
import org.joda.time.DateTime;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/thing")
public class RestApiController {
  Map<String, Map<String, Object>> things = new HashMap<>();
  Map<String, Object> thingsDetail = new HashMap<>();

  public RestApiController() {
    Random rand = new Random();

    things.put("a", ImmutableMap.of("id", "asdasd", "col1", "f89g7fgh", "createdAt", DateTime.now().minusHours(rand.nextInt(5))));
    things.put("b", ImmutableMap.of("id", "fdgfdg", "col1", "i34h5jkg", "createdAt", DateTime.now().minusHours(rand.nextInt(5))));
    things.put("c", ImmutableMap.of("id", "piwoerj", "col1", "98udfijsd", "createdAt", DateTime.now().minusHours(rand.nextInt(5))));
    things.put("d", ImmutableMap.of("id", "ywqjehbkj", "col1", "lkwn34kjrne89", "createdAt", DateTime.now().minusHours(rand.nextInt(5))));
    thingsDetail.put("asdasd", ImmutableMap.of("value", "somedetailsthing"));
    thingsDetail.put("fdgfdg", ImmutableMap.of("value", "blahblahblahblahblah"));
    thingsDetail.put("piwoerj", ImmutableMap.of("value", "moreobjectdetails"));
  }

  @RequestMapping
  public ResponseEntity searchThings(@RequestParam("query") String query) {
    List<Map> results = Arrays.asList(query.split(";")).stream()
        .map((str) -> things.get(str)).filter((res) -> res != null).collect(Collectors.toList());
    return ResponseEntity.ok(results); //random "search results"
  }

  @RequestMapping("/{id}")
  public ResponseEntity getThing(@PathVariable("id") String id) {
    if (thingsDetail.containsKey(id)) {
      return ResponseEntity.ok(thingsDetail.get(id)); //stub "get detail"
    }

    return ResponseEntity.notFound().build();
  }
}
