package jono.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Enable AngularJS HTML5 urls by responding to all pages with the root of the S.P.A. and letting angular display the right view.
 * @author Jonathon Gillett
 */
@Controller
public class WebController {
    @RequestMapping("/")
    public String angular() {
        return "index";
    }
}
