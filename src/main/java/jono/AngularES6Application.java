package jono;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@SpringBootApplication
public class AngularES6Application extends SpringBootServletInitializer {

  public static void main(String[] args) {
    new SpringApplicationBuilder(AngularES6Application.class).run(args);
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    return builder.sources(AngularES6Application.class);
  }
}
