var path = require('path');

module.exports = {
  entry: "./js/init.js",
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/thingtest/dist/',
    filename: 'app.js'
  },
  module: {
    loaders: [
      { test: /\.scss$/, loader: "style!css!sass" },
      { test: /\.css$/, loader: "style!css" },
      { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader",
        query: {
          presets: [require.resolve('babel-preset-es2015')]
        }
      },
      { test: /\.html$/,loader: 'html-loader' },
      {
        test   : /\.(ttf|eot|svg|woff2?)$/,
        loader : 'file-loader',
        query : {
          limit: 10000,
          name: 'fonts/[name].[ext]'
        }
      }
    ]
  }
};