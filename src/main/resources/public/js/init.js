import angular from 'angular';
import './app/angularModuleSetup';

//require the styles (webpack conventions are weird)
import '../css/bootstrap/bootstrap.css'
import '../css/bootstrap/bootstrap-theme.css'
import '../css/ui-grid/ui-grid.min.css';
import '../css/fonts.css';
import '../sass/overrides.scss';
import '../sass/main.scss';

//after page load, start the angular app (replaces standard ng-app behaviour so we can use module loading)
angular.element(document).ready(function() {
  angular.bootstrap(document, ['thingApp']);
});