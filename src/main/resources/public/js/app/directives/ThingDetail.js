import template from './ThingDetail.html';

export default class ThingDetail {
  constructor() {
    this.template = template;
    this.restrict = 'E';
    this.scope = {};

    this.controller = ThingDetailController;
    this.controllerAs = "spre";
    this.bindToController = true;
  }

  static directiveFactory() {
    return new ThingDetail();
  }
}

class ThingDetailController {
  constructor(SearchService, Messages) {
    this.service = SearchService;
    this.Messages = Messages;
    this.selection = SearchService.selection; //bind against services to pass data across controllers
  }
}

export default ThingDetail.directiveFactory;