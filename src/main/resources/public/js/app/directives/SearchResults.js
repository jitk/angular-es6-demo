import template from './SearchResults.html';

export default class SearchResults {
  constructor() {
    this.template = template;
    this.restrict = 'E';
    this.scope = {};

    this.controller = SearchResultsController;
    this.controllerAs = "sres";
    this.bindToController = true;
  }

  static directiveFactory() {
    return new SearchResults();
  }
}

class SearchResultsController {
  constructor(SearchService, Messages, $scope) {
    let self = this;
    this.service = SearchService;
    this.Messages = Messages;
    //you can ignore most of this except 'data' and the 'selectResult' call at the bottom
    this.gridOptions = {
      data: SearchService.results,
      enableColumnMenus: false,
      enableRowSelection: true,
      enableFullRowSelection: true,
      enableSelectAll: false,
      enableRowHeaderSelection: false,
      multiSelect: false,
      noUnselect: true,
      modifierKeysToMultiSelect: false,
      onRegisterApi(gridApi) {
        gridApi.selection.on.rowSelectionChanged($scope,function(row){
          self.service.selectResult(row.entity);
        });
      }
    };
  }
}

export default SearchResults.directiveFactory;