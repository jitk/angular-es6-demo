import template from './SearchFilters.html';

class SearchFilters {
  constructor() {
    this.template = template;
    this.restrict = 'E';
    this.scope = {};

    this.controller = SearchFiltersController;
    this.controllerAs = "srch";
    this.bindToController = true;
  }

  static directiveFactory() {
    return new SearchFilters();
  }
}

class SearchFiltersController {
  constructor(SearchService, Messages) {
    this.service = SearchService;
    this.Messages = Messages;
    this.criteria = {
      query: "a;b"
    };
  }

  search() {
    this.service.performSearch(this.criteria);
  }
}

export default SearchFilters.directiveFactory;