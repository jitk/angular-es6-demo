export default class ThingController {
  constructor(Messages) {
    this.messages = Messages.messages;
  }
  
  removeMessage(idx) {
    this.messages.splice(idx,1)[0].cancelTimeout();
  }
}
