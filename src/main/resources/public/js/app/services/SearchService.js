export default class SearchService {
  constructor($http, $filter, Messages) {
    //used this so that thymeleaf can inject the current contextPath (nice trick to allow deployment in weird tomcat setups etc)
    this.baseUrl = window.serviceBaseUrl;
    this.Messages = Messages;
    this.$http = $http;
    this.$filter = $filter;
    this.results = [];
    this.selection = {result: null};
  }

  setResults(newResults) {
    //clear and then re-fill the results array to avoid swapping references out and losing the databind
    this.results.length = 0;
    Array.prototype.push.apply(this.results, newResults); //essentially "pushAll"
  }

  selectResult(selectedResult) {
    this.selection.result = selectedResult; // need to set a sub-field on the selection object to maintain databind
    this.getThingDetails(this.selection.result)
        .then(
            (res) => this.selection.details = res.data,
            (err) => this.Messages.alert({message: "Failed to get thing detail", error: err.data.error})
        );
  }

  getThingDetails(thing) {
    return this.$http.get(this.baseUrl+"api/thing/"+thing.id);
  }
  
  performSearch(criteria) {
    this.$http.get(this.baseUrl+"api/thing"+this.criteriaToQueryParams(criteria))
        .then(
            (res) => this.setResults(res.data),
            (err) => this.Messages.alert({message: "Failed to perform search!", error: err.data.error})
        );
  }

  criteriaToQueryParams(criteria) {
    return '?query='+encodeURIComponent(criteria.query);
  }
}