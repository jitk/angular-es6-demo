const messageTimeouts = {
  danger: 8000, //8 secs
  info: 3000    //3 secs
};
//MessageService is used to display errors/alerts consistently from anywhere in the app
export default class MessageService {
  constructor($timeout) {
    this.$timeout = $timeout;
    this.messages = [];
  }

  alert(error) {
    this._add({type: 'danger', message: error.message, error: error.error});
  }
  
  info(msg) {
    this._add({type: 'info', message: msg});
  }

  _add(message) {
    this.messages.push(message);
    //auto-remove after X seconds
    let interval = messageTimeouts[message.type];
    let timeout = this.$timeout(() => {
      let idx = this.messages.indexOf(message);
      this.messages.splice(idx, 1);
    }, interval);
    //attach timeout cancel callback so manual remove can cancel the auto remove
    message.cancelTimeout = () => this.$timeout.cancel(timeout);
  }
}