import angular from 'angular';
import 'angular-ui-bootstrap';
import '../lib/angular/ui-grid.min';

import ThingController from './controllers/ThingController';

import MessageService from './services/MessageService';
import SearchService from './services/SearchService';

import SearchFilters from './directives/SearchFilters';
import SearchResults from './directives/SearchResults';
import ThingDetail from './directives/ThingDetail';

export default angular.module('thingApp', ['ui.bootstrap', 'ui.bootstrap.datepicker', 'ui.grid', 'ui.grid.selection'])
    //controllers and services just expose the class because they can be constructed normally
    .controller('thingCtrl', ThingController)
    .service('Messages', MessageService)
    .service('SearchService', SearchService)
    //directives all use the static builder method convention
    .directive('thingSearchFilters', SearchFilters)
    .directive('thingSearchResults', SearchResults)
    .directive('thingDetail', ThingDetail);