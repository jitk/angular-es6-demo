# AngularJS ES6 Demo app
## To Run
1. Start Webpack:
  - go to src/main/resources/public and run `webpack -w` to start webpack in watch mode to pick up any changes (without the -w for single use)
2. Start Web service
  - from the project root run `./gradlew bootRun` to start the webservice on port 8081
3. Visit https://localhost:8081/thingtest to view the app

## Service
The Service is a super simple Spring Boot app, with 2 controllers.

### RestApiController
This serves up the actual data/API endpoints.

### WebController
This allows Angular to use HTML5 location instead of page fragments (e.g. #!/path).

Basically just add mapping to any page that AngularJS has in its routing, and you'll be able to link directly to it.

_Otherwise you'll get 404's if you refresh the page or send someone a link from your browser bar_

## Front-end
### Templates
I've decided to use Thymeleaf because I'm not using enough templating features to do a proper evaluation.

I'm only using it to get URL links to resources which include the context-path without having to hard-code it.

I also use this to embed the context-path directly into a global JS variable for the AngularJS services to use to
generate correct paths for API calls.

### AngularJS Layout
- **css/** contains all the static css, including from libraries
- **dist/** contains the output from webpack - _this is what gets served in the browser_
- **js/** contains the javascript for the controllers, services and directives; as well as the overall 'module'

#### Bootstrap
Because of ES6 and modules, the standard Angular ng-app doesn't work.

To solve this, I've got the main entry-point of the JS - init.js

init.js simply imports all the things needed to make the app go. This includes:

- stylesheets
- the angular library
- our angular app/module

And after all the imports it just waits for the page to load, then manually bootstraps us in to start the normal process.

#### App Structure
##### Controllers
This app mainly uses directive, and I've chosen to maintain their controllers internally.

You could just as easily have them here and require across, but since they're so tightly coupled it didn't seem useful.

##### Services
The Services are one of the most important parts of the app.

They perform the API calls, hold all the shared app state and enable data to pass between controllers/directives with ease.

The `MessageService` provides a consistent way for any part of the app to trigger an alert.

The `SearchService` holds the current results, selection and details, and facilitates the API calls for
search and detail lookup.

##### Directives
All of the business logic is wrapped inside the directive objects, in order to provide some separation of concerns.

There is a filters/search panel directive, a search results directive and a detail directive.

The use of the controller function and `export default MyDirective.directiveFactory()` pattern I used was taken from
this StackOverflow answer [here](http://stackoverflow.com/a/34281655/5619843).
It does theoretically over-expose your directive's controller methods, but it's significantly cleaner than the other
solutions.

To enable the templates to be required, I used html-loader which essentially transforms the HTML into a string to be
exposed to JS. Since AngularJS accepts string tempaltes that was all I needed.
This also mean there are no async templateUrl issues, and the whole app.js build output can be compressed and cached.